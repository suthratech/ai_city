'use strict';

module.exports = function(Nelloretrafficvehicle) {
Nelloretrafficvehicle.getAllData = function(cb){
Nelloretrafficvehicle.find(function(err,res){
    if(err){
        cb(err)
    }else{
        let allData = {
            cars:0,
            bus:0,
            motor_bikes:0,
            emergency:0,
            auto:0,
            riders:0,
            trucks:0,
            pedestrians:0,
            data:[]
        }
        let resp = JSON.parse(JSON.stringify(res));
        if(resp.length==0){
            cb(null,allData);
        }else{
            allData.data = resp;
            resp.filter(ele=>{
                ele.vehicle_type=='car'?allData.cars+=1:ele.vehicle_type=='bus'?allData.bus+=1:ele.vehicle_type=='bike'?allData.motor_bikes+=1:ele.vehicle_type=='truck'?allData.trucks+=1:ele.vehicle_type=='auto'?allData.auto+=1:'';
                ele.vehicle_type=='rider'?allData.riders+=1:ele.vehicle_type=='pedestrians'?allData.pedestrians+=1:ele.emergency_type!=""?allData.emergency+=1:'';
            });
            cb(null,allData);
        }
    }
})
}
Nelloretrafficvehicle.remoteMethod('getAllData', {
    http: { path: '/getAllData', verb: 'get' },
    returns: { arg: 'response', type: 'any' }
});
};
